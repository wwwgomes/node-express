const express = require('express')
const router = express.Router()
const PersonController = require('../controllers/PersonController')

router.post('/', PersonController.post)
router.put('/:id', PersonController.put)
router.delete('/:id', PersonController.delete)

module.exports = router
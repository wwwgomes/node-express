const express = require('express')
const app = express()

const router = express.Router()
const index = require('./routes/index')
const PersonRouter = require('./routes/PersonRoute')

app.use('/', index)
app.use('/person', PersonRouter)

module.exports = app